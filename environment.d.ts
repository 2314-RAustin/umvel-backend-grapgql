namespace NodeJS {
    interface ProcessEnv {
        DB_HOST: string;
        DB_NAME: string;
        DB_DIALECT: string;
        DB_USER: string;
        DB_PASSWORD: string;
        DB_PORT: number;
        DB_ROOT_PASSWORD: string;
        DB_SYNCHRONIZE: boolean;
        NODE_PORT: number;
        NODE_ENV: string;
    }
}
