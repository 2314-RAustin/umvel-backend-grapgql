import { GraphQLObjectType, GraphQLSchema } from 'graphql';
import { CREATE_TICKET, UPDATE_TICKET_TOTAL } from './Mutations/ticket';
import { GET_ALL_TICKETS, GET_TICKET } from './Queries/ticket';

const RootQuery = new GraphQLObjectType({
    name: 'RootQuery',
    fields: {
        get_all_tickets: GET_ALL_TICKETS,
        get_ticket: GET_TICKET,
    }
});
const Mutation = new GraphQLObjectType({
    name: 'Mutation',
    fields: {
        create_ticket: CREATE_TICKET,
        update_ticket_total: UPDATE_TICKET_TOTAL,
    }
});

export const schema = new GraphQLSchema({
    query: RootQuery,
    mutation: Mutation
});