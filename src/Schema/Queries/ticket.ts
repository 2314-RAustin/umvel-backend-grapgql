import { GraphQLID, GraphQLList } from 'graphql';
import { Ticket } from '../TypeDefs';
import { Tickets } from '../../Entities';

export const GET_ALL_TICKETS = {
    type: new GraphQLList(Ticket),
    resolve() {
        return Tickets.find();
    }
};

export const GET_TICKET = {
    type: Ticket,
    args: {
        id: { type: GraphQLID },
    },
    resolve(_parent: unknown, args: { id: number }) {
        const { id } = args;
        return Tickets.findOne({ id });
    }
};