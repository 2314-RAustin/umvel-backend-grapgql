import { GraphQLFloat, GraphQLID } from 'graphql';
import  { Tickets }  from '../../Entities';
import { Ticket } from '../TypeDefs';
import getTicketByID from '../../ApolloClient/querys/getTicketByID';

export const CREATE_TICKET = {
    type: Ticket,
    args: {
        subtotal: { type: GraphQLFloat }
    },
    resolve(_parent:unknown, args: { subtotal: number }): Promise<unknown> {
        const ticket = new Tickets();
        ticket.subtotal = parseFloat((args.subtotal).toFixed(2));
        return ticket.save();
    }
};

export const UPDATE_TICKET_TOTAL = {
    type: Ticket,
    args: {
        id: { type: GraphQLID },
    },
    async resolve(_parent: unknown, args: {  id: number }) {
        const { id } = args;
        const response = await getTicketByID(id);

        if (!response.loading && response.data?.get_ticket?.subtotal >= 0) {
            const { subtotal } = response.data.get_ticket;
            const total = parseFloat((subtotal * 0.16 + subtotal).toFixed(2));
            await Tickets.update({ id }, { total });
            
            return {
                id,
                subtotal,
                total,
            };
        }
    }
};