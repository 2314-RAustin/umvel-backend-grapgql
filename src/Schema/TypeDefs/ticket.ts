import { GraphQLFloat, GraphQLID, GraphQLObjectType } from 'graphql';

export const Ticket = new GraphQLObjectType({
    name: 'Ticket',
    fields: () => ({
        id: { type: GraphQLID },
        subtotal: { type: GraphQLFloat },
        total: { type: GraphQLFloat }
    })
});