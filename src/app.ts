import express, { Request, Response, NextFunction } from 'express';
import { graphqlHTTP } from 'express-graphql';
import cors from 'cors';
import { schema } from './Schema';

export const app = async (port: number | string | false) => {
    const expressApp = express();

    expressApp.set('port', port);
    expressApp.use(cors());
    expressApp.use(express.json());
    expressApp.use(express.urlencoded({ extended: false }));
    expressApp.get('/health', (_req, res) => res.status(200).json({ alive:true }));

    expressApp.use((err: Error, req: Request, res: Response, next: NextFunction) => {
        res.locals.message = err.message;
        res.locals.error = req.app.get('env') === 'dvelopment' ? err : {};
        res.status(500);
        res.render('error');
    });

    expressApp.use(
        '/graphql/v1',
        graphqlHTTP({
            schema,
            graphiql: true
        })
    );

    return expressApp;
};