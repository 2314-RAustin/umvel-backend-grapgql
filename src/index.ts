/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/no-var-requires */
import * as R from 'ramda';
import path from 'path';
import http from 'http';
import cluster from 'cluster';
import { app } from './app';
import { db } from './Services/Database';
import dotenvSafe from 'dotenv-safe';
import os from 'os';

dotenvSafe.config({
    allowEmptyValues: true, 
    example: path.resolve(__dirname, '../.env.example'),
    path: path.resolve(__dirname, '../.env'),
});

const numCPUs = os.cpus.length;
const debug = require('debug')('be-template:server');

const normalizePort = (val: string|number) => {
    const portValue = parseInt(val.toString(), 10);
    if (isNaN(portValue)) {
        return val;
    }

    if (portValue >= 0) {
        return portValue;
    }

    return false;
};

const onError = (port: number|string|false) => {
    return (error:any) => {
        if (error.syscall !== 'listen' ) {
            throw error;
        }
        const bind = typeof port === 'string' ? 'S ' + port : 'Port ' + port;
        // handle error
        switch(error.code) {
        case 'EACCESS':
            console.error(bind + ' requires elevates privilegs');
            process.exit(1);
            break;
        case 'EADDRINUSE':
            console.error(bind + ' is already in use');
            process.exit(1);
            break;
        default:
            throw error;
        }
    };
};

const onListenign = (server: http.Server) => {
    return () => {
        const addr = server.address();
        const bind = typeof addr === 'string' ? 'S ' + addr : 'Port ' + addr?.port;
        debug('Listening on ' + bind);
    };
};

const boot = async () => {
    const port = normalizePort(process.env.NODE_PORT || '3001' );
    await db.connect().then(()=>{
        app(port)
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
            .then((expressSettings: any)=> {
                const server = http.createServer(expressSettings);
                server.listen(port);
                server.on('error', onError(port));
                server.on('listening', onListenign(server));
                console.info(`worker ${process.pid} started. Listening on port ${port}`);
            })
            .catch(console.error);
    });
};

async function main() {
    if(!cluster.isPrimary && !R.includes(process.env.NODE_ENV, ['', 'LOCAL'])) {
        console.info(`MASTER ${process.pid} is running`);
        for(let i = 0; i < numCPUs; i++) {
            cluster.fork();
        }
        cluster.on('exit', (worker) => {
            console.info(`worker ${worker.process.pid} died`);
        });
    } else {
        await boot();
    }
}


main();