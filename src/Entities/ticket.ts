import { BaseEntity, Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Tickets extends BaseEntity {

    @PrimaryGeneratedColumn()
        id!: number;

    @Column({ type: 'numeric', precision: 65, scale: 2, })
        subtotal!: number;

    @Column({ type: 'numeric', precision: 65, scale: 2, })
        total!: number;
}