import { createConnection } from 'typeorm';
import * as Entities from '../Entities';
import { connection } from '../Services/Database/settings';


export const db = {
    connect: () => 
        createConnection({
            ...connection,
            type: 'mysql',
            port: Number(connection.port) ,
            entities: Object.values(Entities),
        })
};