import { Connection } from 'typeorm';
import { db } from './createTestConnection';
import '../index';
import gql from 'graphql-tag';
import crossFetch from 'cross-fetch';
import { ApolloClient } from 'apollo-client';
import { createHttpLink } from 'apollo-link-http';
import { InMemoryCache } from 'apollo-cache-inmemory';
import dotenv from 'dotenv';
dotenv.config();

const {
    DB_HOST,
    NODE_PORT,
} = process.env;

const link = createHttpLink({
    uri: `http://${DB_HOST}:${NODE_PORT}/graphql/v1`,
    fetch: crossFetch as any
});

const client = new ApolloClient({
    link,
    cache: new InMemoryCache()
});

const createTicketMutations = (subtotal: number) => ({
    mutation: gql`
        mutation {
            create_ticket(subtotal: ${subtotal}) {
                id
                subtotal
                total
            }
        }
    `
});

const updateTicketMutation = (id: number) => ({
    mutation: gql`
        mutation {
            update_ticket_total (id: ${id}) {
                id
                subtotal
                total
            }
        }
    `
});

const getAllTicketsQuery = () => ({
    query: gql`
        query {
            get_all_tickets {
                id
                subtotal
                total
            }
        }
    `
});

const getTicketQuery = (id: number) => ({
    query: gql`
        query {
            get_ticket(id: ${id}) {
                id
                subtotal
                total
            }
        }
    `
});

let conn: Connection;

beforeAll(async () => {
    conn = await db.connect();
});

afterAll((done) => {
    conn.close();
    done();
});

jest.retryTimes(3);

describe('Querys / tickets', () => {
    test('should get a single ticket', async () => {
        const response = await client.query(getTicketQuery(9));
        expect(typeof response.data?.get_ticket).toBe('object');
    });
    
    test('should get all tickets', async () => {
        const response = await client.query(getAllTicketsQuery());
        expect(Array.isArray(response.data?.get_all_tickets)).toBe(true);
    });
});

describe('Mutations / tickets', () => {
    test('should create a ticket', async () => {
        const response = await client.mutate(createTicketMutations(100));
        expect(typeof response.data?.create_ticket).toBe('object');
        expect(Object.keys(response.data?.create_ticket).length).toBe(4);
        const { subtotal, total } = response.data.create_ticket;
        expect(subtotal).toBe(100);
        expect(total).toBe(null);
    });

    test('should update ticker with IVA', async () => {
        const responseUpdated = await client.mutate(updateTicketMutation(68));
        expect(typeof responseUpdated.data?.update_ticket_total).toBe('object');
        expect(Object.keys(responseUpdated.data?.update_ticket_total).length).toBe(4);
        const { total: totalAfterUpdated, subtotal } = responseUpdated.data.update_ticket_total;
        const totalCalculated =  subtotal * 0.16 + subtotal;
        expect(totalAfterUpdated).toBe(totalCalculated);
    });

    test('should create ticket and update ticket with IVA', async () => {
        const response = await client.mutate(createTicketMutations(100));
        expect(typeof response.data?.create_ticket).toBe('object');
        expect(Object.keys(response.data?.create_ticket).length).toBe(4);
        const { id, subtotal, total } = response.data.create_ticket;
        expect(subtotal).toBe(100);
        expect(total).toBe(null);
        const responseUpdated = await client.mutate(updateTicketMutation(id));
        expect(typeof responseUpdated.data?.update_ticket_total).toBe('object');
        expect(Object.keys(responseUpdated.data?.update_ticket_total).length).toBe(4);
        const { total: totalAfterUpdated } = responseUpdated.data.update_ticket_total;
        const totalCalculated =  subtotal * 0.16 + subtotal;
        expect(totalAfterUpdated).toBe(totalCalculated);
    });
});
