import { createConnection } from 'typeorm';
import { connection } from './settings';
import { Configs, Tickets  } from '../../Entities';

export const db = {
    connect: () => 
        createConnection({
            ...connection,
            type: 'mysql',
            port: Number(connection.port),
            entities: [Configs, Tickets],
        })
            .catch(console.error),
};