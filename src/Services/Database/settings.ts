import Path from 'path';
import dotenv from 'dotenv-safe';

Promise.resolve(
    dotenv.config({
        allowEmptyValues:true,
        path: Path.resolve(process.cwd(),'.env')
    })
);

const { DB_PORT, DB_DIALECT, DB_HOST, DB_NAME, DB_PASSWORD, DB_USER, NODE_ENV } = process.env;

export const connection = {
    name: 'default',
    type: DB_DIALECT,
    host: DB_HOST,
    port: DB_PORT,
    database: DB_NAME,
    password: DB_PASSWORD,
    username: DB_USER,
    logging: NODE_ENV === 'production',
    synchronize: true
};