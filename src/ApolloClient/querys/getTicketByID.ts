import gql from 'graphql-tag';
import crossFetch from 'cross-fetch';
import { ApolloClient } from 'apollo-client';
import { createHttpLink } from 'apollo-link-http';
import { InMemoryCache } from 'apollo-cache-inmemory';
import dotenv from 'dotenv';
dotenv.config();

const { DB_HOST, NODE_PORT } = process.env;

const link = createHttpLink({
    uri: `http://${DB_HOST}:${NODE_PORT}/graphql/v1`,
    fetch: crossFetch,
});

const client = new ApolloClient({
    link,
    cache: new InMemoryCache(),
});

export default (id: number) => {
    return client.query({
        query: gql`
        query {
          get_ticket(id: ${id}) {
            id
            subtotal
            total
          }
        }
    `,
    });
};
